import os
import tempfile
import traceback

from flask import Flask, request
from piperci.faas.exceptions import PiperError
from piperci.faas.this_task import ThisTask
from werkzeug.exceptions import BadRequest

from .config import Config
from . import handler

app = Flask(__name__)


@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """

    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == "chunked":
        request.environ["wsgi.input_terminated"] = True


@app.route("/", defaults={"path": ""}, methods=["POST"])
@app.route("/<path:path>", methods=["POST", "GET"])
def main_route(path):
    Config["path"] = path
    Config["endpoint"] = request.headers.get('X-Forwarded-Host')

    with tempfile.TemporaryDirectory() as tempdir:
        os.chdir(tempdir)
        try:
            json = request.get_json(force=True)
        except BadRequest:
            return f"JSON must be supplied in request", 422

        try:
            data = {
                "storage": {
                    "storage_type": "minio",
                    "access_key": Config["storage"]["access_key"],
                    "secret_key": Config["storage"]["secret_key"],
                    "hostname": Config["storage"]["url"],
                },
                "caller": Config["name"],
                "gman_url": Config["gman"]["url"],
                "status": "started" if Config["type"] == "gateway" else "received",
                "thread_id": json.get("thread_id"),
                "parent_id": json.get("parent_id"),
                "project": json["project"],
                "run_id": json["run_id"],
                "stage": json["stage"],
            }
            task = ThisTask(**data)
        except PiperError as e:
            return f"There was an error creating task {e}", 422
        except KeyError:
            message = traceback.format_exc()
            return f"Invalid JSON detected. {message}", 422

        try:
            return handler.handle(request, task=task, config=Config)
        except Exception:
            error = traceback.format_exc()
            message = f"Unknown error processing function. \n{error}"
            task.fail(message)
            return message, 422
